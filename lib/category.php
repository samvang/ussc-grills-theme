<?php
/**
 * Filename posts.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Category;

function get_featured_posts( $category_term ) {
	global $wp_query;

	if ( ! $wp_query->is_tax( 'ussc_recipe_cat' ) ) {
		return [];
	}

	if ( null === $category_term ) {
		$category_term = $wp_query->get_queried_object();
	}

	$featured_posts = get_field( 'featured_posts', $category_term->taxonomy . '_' . $category_term->term_id );

	if ( ! $featured_posts ) {
		// Get the latest post.
		$featured_query = new \WP_Query( [
			'posts_per_page'         => 1,
			'tax_query'              => [
				[
					'taxonomy' => 'ussc_recipe_cat',
					'term_id'  => $category_term->term_id,
				],
			],
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		] );
		$featured_posts = $featured_query->get_posts();
	} else {
		// Flatten the array, Gets rid of the repeater dimension.
		$featured_posts = array_map( function ( $featured_post ) {
			return $featured_post['featured_post'];
		}, $featured_posts );
	}

	return ( is_array( $featured_posts ) )
		? $featured_posts
		: [];
}
