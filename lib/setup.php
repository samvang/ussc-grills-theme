<?php

namespace USSC\Theme\Setup;

use USSC\Theme\Assets;

/**
 * Theme setup
 */
function setup() {
	// Enable features from Soil when plugin is activated
	// https://roots.io/plugins/soil/.
	add_theme_support( 'soil-clean-up' );
	add_theme_support( 'soil-disable-asset-versioning' );
	add_theme_support( 'soil-disable-trackbacks' );
	add_theme_support( 'soil-jquery-cdn' );
//	add_theme_support( 'soil-js-to-footer' );
	add_theme_support( 'soil-nav-walker' );
	add_theme_support( 'soil-nice-search' );
	add_theme_support( 'soil-relative-urls' );

	// Make theme available for translation
	// Community translations can be found at https://github.com/roots/sage-translations.
	load_theme_textdomain( 'usstove', get_template_directory() . '/lang' );

	// Enable plugins to manage the document title
	// http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag.
	add_theme_support( 'title-tag' );

	// Register wp_nav_menu() menus
	// http://codex.wordpress.org/Function_Reference/register_nav_menus.
	register_nav_menus( [
		'top_nav_l'    => __( 'Top Navigation - Left', 'usstove' ),
		'top_nav_r'    => __( 'Top Navigation - Right', 'usstove' ),
		'main_nav_l'   => __( 'Main Navigation - Left', 'usstove' ),
		'main_nav_r'   => __( 'Main Navigation - Right', 'usstove' ),
		'footer_nav_1' => __( 'Footer Navigation - Col 1', 'usstove' ),
		'footer_nav_2' => __( 'Footer Navigation - Col 2', 'usstove' ),
		'footer_nav_3' => __( 'Footer Navigation - Col 3', 'usstove' ),
		'footer_nav_4' => __( 'Footer Navigation - Col 4', 'usstove' ),
		'mobile_nav'   => __( 'Mobile Navigation', 'usstove' ),
	] );

	// Enable post thumbnails
	// http://codex.wordpress.org/Post_Thumbnails
	// http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
	// http://codex.wordpress.org/Function_Reference/add_image_size.
	add_theme_support( 'post-thumbnails' );

	// Enable image sizes.
	add_image_size( 'brand-logo', 180, 999, false );
	add_image_size( 'card-thumb-cat', 420, 200, false );
	add_image_size( 'card-thumb-prod', 420, 200, false );
	add_image_size( 'card-thumb-page', 420, 200, false );
	add_image_size( 'card-thumb-brand', 420, 200, false );
	add_image_size( 'card-thumb-design', 373, 300, true );
	add_image_size( 'mega-menu-thumb', 90, 90, false );
	add_image_size( 'cart-thumb', 90, 90, false );

	// Enable HTML5 markup support
	// http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5.
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );

	// Use main stylesheet for visual editor
	// To add custom styles edit /assets/styles/layouts/_tinymce.scss.
	add_editor_style( Assets\asset_path( 'styles/tinymce.css' ) );
}

add_action( 'after_setup_theme', __NAMESPACE__ . '\\setup' );

/**
 * Theme assets
 * Cache busting via hash added to filename, so version query string not required.
 */
function assets() {

	// Dequeue Jetpack Infinite Scroll styles, we use our own.
	wp_dequeue_style( 'the-neverending-homepage' );

	if ( is_admin() ) {
		wp_enqueue_style( 'usstove-admin', Assets\asset_path( 'styles/admin.css' ), [], false ); // phpcs:ignore
	}

	wp_enqueue_style( 'usstove-main', Assets\asset_path( 'styles/main.css' ), [], false ); // phpcs:ignore

	wp_register_script( 'usstove-main', Assets\asset_path( 'scripts/main.js' ), [ 'jquery' ], false, true ); // phpcs:ignore
	wp_enqueue_script( 'usstove-main' );
}

add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100 );

/**
 * Admin assets
 * Cache busting via hash added to filename, so version query string not required.
 */
function admin_assets() {
	wp_enqueue_style( 'usstove/admin', Assets\asset_path( 'styles/admin.css' ), [], false ); // phpcs:ignore
}

add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\admin_assets', 100 );

/**
 * Add Options Pages for customizing global settings on US Stove.
 */
function add_options_page() {

	if ( function_exists( 'acf_add_options_page' ) ) {

		\acf_add_options_page( array(
			'page_title' => __( 'US Stove', 'usstove' ),
			'menu_title' => __( 'US Stove', 'usstove' ),
			'menu_slug'  => 'usstove',
			'capability' => 'manage_options',
			'redirect'   => true,
		) );

		\acf_add_options_sub_page( array(
			'parent_slug' => 'usstove',
			'page_title'  => __( 'General', 'usstove' ),
			'menu_title'  => __( 'General', 'usstove' ),
			'menu_slug'   => 'usstove-general',
			'capability'  => 'manage_options',
		) );

		\acf_add_options_sub_page( array(
			'parent_slug' => 'usstove',
			'page_title'  => __( 'Header', 'usstove' ),
			'menu_title'  => __( 'Header', 'usstove' ),
			'menu_slug'   => 'usstove-header',
			'capability'  => 'manage_options',
		) );

		\acf_add_options_sub_page( array(
			'parent_slug' => 'usstove',
			'page_title'  => __( 'Footer', 'usstove' ),
			'menu_title'  => __( 'Footer', 'usstove' ),
			'menu_slug'   => 'usstove-footer',
			'capability'  => 'manage_options',
		) );

		\acf_add_options_sub_page( array(
			'parent_slug' => 'usstove',
			'page_title'  => __( 'Pencil Ad', 'usstove' ),
			'menu_title'  => __( 'Pencil Ad', 'usstove' ),
			'menu_slug'   => 'usstove-pencil-ad',
			'capability'  => 'manage_options',
		) );

		\acf_add_options_sub_page( array(
			'parent_slug' => 'usstove',
			'page_title'  => __( 'Mega Menus', 'usstove' ),
			'menu_title'  => __( 'Mega Menus', 'usstove' ),
			'menu_slug'   => 'usstove-mega-menu',
			'capability'  => 'manage_options',
		) );

		\acf_add_options_sub_page( array(
			'parent_slug' => 'usstove',
			'page_title'  => __( 'Retailers', 'usstove' ),
			'menu_title'  => __( 'Retailers', 'usstove' ),
			'menu_slug'   => 'usstove-retailers',
			'capability'  => 'manage_options',
		) );


		\acf_add_options_sub_page( array(
			'parent_slug' => 'usstove',
			'page_title'  => __( 'Analytics / GTM', 'usstove' ),
			'menu_title'  => __( 'Analytics / GTM', 'usstove' ),
			'menu_slug'   => 'usstove-gtm',
			'capability'  => 'manage_options',
		) );

	}
}

add_action( 'admin_menu', __NAMESPACE__ . '\\add_options_page' );

/**
 * Add SVG mime-type to list of supported types.
 *
 * @param array $mimes List of mime-types.
 *
 * @return mixed Revised list of mime-types with SVG added.
 */
function mime_types( $mimes ) {
	$mimes['svg'] = 'image/svg+xml';

	return $mimes;
}

add_filter( 'upload_mimes', __NAMESPACE__ . '\\mime_types' );

/**
 * Fix for display of SVG thumbnails in Media panel.
 */
function svg_admin_styles() {
	echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}

add_action( 'admin_head', __NAMESPACE__ . '\\svg_admin_styles' );
