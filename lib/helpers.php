<?php
/**
 * Filename helpers.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Helpers;

use USSC\Theme\Assets;

/**
 * Get URL for a tile representing a page (or post) thumbnail.
 *
 * @param int    $post_id    The Post's I.D
 * @param string $image_size The image size.
 * @param bool   $fallback   Fallback to placeholder or not.
 *
 * @return string The URL.
 */
function get_page_tile_thumbnail( $post_id, $image_size, $fallback = true ) {
	$thumb = get_the_post_thumbnail( $post_id, $image_size );

	if ( ! $thumb && $fallback ) {
		$thumb = sprintf( '<img src="%s">',
			esc_attr( Assets\asset_path( 'images/product-placeholder.png' ) )
		);
	}

	return $thumb;
}

/**
 * Get URL for a tile representing a brand thumbnail.
 *
 * @param \WP_Term $brand      The Brand \WP_Term.
 * @param string   $image_size The image size.
 * @param bool     $fallback   Fallback to placeholder or not.
 *
 * @return string The URL.
 */
function get_brand_tile_thumbnail( $brand, $image_size, $fallback = true ) {
	$logo = get_field( 'logo', $brand );

	$thumb = wp_get_attachment_image( $logo['ID'], 'card-thumb-brand' );

	if ( ! $thumb && $fallback ) {
		$thumb = sprintf( '<img src="%s">',
			esc_attr( Assets\asset_path( 'images/product-placeholder.png' ) )
		);
	}

	return $thumb;
}
