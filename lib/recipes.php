<?php
/**
 * Filename posts.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Recipes;

// Register Custom Post Type
add_action( 'init', function () {

	$labels = array(
		'name'                  => _x( 'Recipes', 'Post Type General Name', 'ussc' ),
		'singular_name'         => _x( 'Recipe', 'Post Type Singular Name', 'ussc' ),
		'menu_name'             => __( 'Recipes', 'ussc' ),
		'name_admin_bar'        => __( 'Recipe', 'ussc' ),
		'archives'              => __( 'Recipe Archives', 'ussc' ),
		'attributes'            => __( 'Recipe Attributes', 'ussc' ),
		'parent_item_colon'     => __( 'Parent Recipe:', 'ussc' ),
		'all_items'             => __( 'All Recipes', 'ussc' ),
		'add_new_item'          => __( 'Add New Recipe', 'ussc' ),
		'add_new'               => __( 'Add New', 'ussc' ),
		'new_item'              => __( 'New Recipe', 'ussc' ),
		'edit_item'             => __( 'Edit Recipe', 'ussc' ),
		'update_item'           => __( 'Update Recipe', 'ussc' ),
		'view_item'             => __( 'View Recipe', 'ussc' ),
		'view_items'            => __( 'View Recipes', 'ussc' ),
		'search_items'          => __( 'Search Recipe', 'ussc' ),
		'not_found'             => __( 'Not found', 'ussc' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'ussc' ),
		'featured_image'        => __( 'Featured Image', 'ussc' ),
		'set_featured_image'    => __( 'Set featured image', 'ussc' ),
		'remove_featured_image' => __( 'Remove featured image', 'ussc' ),
		'use_featured_image'    => __( 'Use as featured image', 'ussc' ),
		'insert_into_item'      => __( 'Insert into recipe', 'ussc' ),
		'uploaded_to_this_item' => __( 'Uploaded to this recipe', 'ussc' ),
		'items_list'            => __( 'Recipes list', 'ussc' ),
		'items_list_navigation' => __( 'Recipes list navigation', 'ussc' ),
		'filter_items_list'     => __( 'Filter recipes list', 'ussc' ),
	);
	$args   = array(
		'label'               => __( 'Recipe', 'ussc' ),
		'description'         => __( 'Recipe', 'ussc' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'          => array( 'ussc_recipe_cat' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_icon'           => 'dashicons-carrot',
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => false,
		'rewrite'             => [ 'slug' => 'recipe' ],
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	register_post_type( 'ussc_recipe', $args );

} );

add_action( 'init', function () {

	$labels = array(
		'name'                       => _x( 'Recipe Categories', 'Taxonomy General Name', 'ussc' ),
		'singular_name'              => _x( 'Recipe Category', 'Taxonomy Singular Name', 'ussc' ),
		'menu_name'                  => __( 'Recipe Categories', 'ussc' ),
		'all_items'                  => __( 'All Items', 'ussc' ),
		'parent_item'                => __( 'Parent Item', 'ussc' ),
		'parent_item_colon'          => __( 'Parent Item:', 'ussc' ),
		'new_item_name'              => __( 'New Item Name', 'ussc' ),
		'add_new_item'               => __( 'Add New Item', 'ussc' ),
		'edit_item'                  => __( 'Edit Item', 'ussc' ),
		'update_item'                => __( 'Update Item', 'ussc' ),
		'view_item'                  => __( 'View Item', 'ussc' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'ussc' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'ussc' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'ussc' ),
		'popular_items'              => __( 'Popular Items', 'ussc' ),
		'search_items'               => __( 'Search Items', 'ussc' ),
		'not_found'                  => __( 'Not Found', 'ussc' ),
		'no_terms'                   => __( 'No items', 'ussc' ),
		'items_list'                 => __( 'Items list', 'ussc' ),
		'items_list_navigation'      => __( 'Items list navigation', 'ussc' ),
	);
	$args   = array(
		'labels'            => $labels,
		'hierarchical'      => true,
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud'     => true,
		'rewrite'           => [
			'slug' => 'recipe-category',
		],
	);

	register_taxonomy(
		'ussc_recipe_cat',
		'ussc_recipe',
		$args
	);
} );


function get_carousel_posts() {
	$carousel_posts = get_field( 'carousel_posts' );

	if ( ! $carousel_posts ) {
		// Get the latest post.
		$carousel_posts = get_posts( [
			'posts_per_page'         => 1,
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		] );
	} else {
		// Flatten the array, Gets rid of the repeater dimension.
		$carousel_posts = array_map( function ( $carousel_post ) {
			return $carousel_post['carousel_post'];
		}, $carousel_posts );
	}

	return ( is_array( $carousel_posts ) )
		? $carousel_posts
		: [];
}

function get_featured_posts() {
	$featured_posts = get_field( 'featured_posts' );

	if ( ! $featured_posts ) {
		// Get the latest post.
		$featured_posts = get_posts( [
			'posts_per_page'         => 1,
			'no_found_rows'          => true,
			'update_post_meta_cache' => false,
			'update_post_term_cache' => false,
		] );
	} else {
		// Flatten the array, Gets rid of the repeater dimension.
		$featured_posts = array_map( function ( $featured_post ) {
			return $featured_post['featured_post'];
		}, $featured_posts );
	}

	return ( is_array( $featured_posts ) )
		? $featured_posts
		: [];
}

//function filter_featured_posts( $query ) {
//	if ( ! is_home() ) {
//		return;
//	}
//
//	remove_action( 'pre_get_posts', __NAMESPACE__ . '\\filter_featured_posts' );
//	$featured_posts = get_featured_posts();
//	add_action( 'pre_get_posts', __NAMESPACE__ . '\\filter_featured_posts' );
//
//	$ppp = get_option( 'posts_per_page' );
//
//	$query->set( 'posts_post_page', get_option( 'posts_per_page' ) + count( $featured_posts ) );
//
//	if ( $query->is_paged ) {
//		$page_offset = ( $query->query_vars['paged'] - 1 ) * $ppp;
//		$query->set( 'offset', $page_offset );
//	}
//
//	return $query;
//}
//
//add_action( 'pre_get_posts', __NAMESPACE__ . '\\filter_featured_posts' );
//
//function adjust_offset_pagination( $found_posts, $query ) {
//
//	$featured_posts = get_featured_posts();
//
//	if ( $query->is_home() ) {
//		return $found_posts - count( $featured_posts );
//	}
//
//	return $found_posts;
//}
//
//add_filter( 'found_posts', __NAMESPACE__ . '\\adjust_offset_pagination', 1, 2 );

