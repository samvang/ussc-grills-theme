<?php
/**
 * Filename options.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Options;

/**
 * Get Analytics Options with reasonable defaults.
 *
 * @return array
 */
function get_analytics_options() {
	$defaults = [
		USSTOVE_ENV_PROD  => [
			'head_scripts'   => '',
			'body_scripts'   => '',
			'footer_scripts' => '',
		],
		USSTOVE_ENV_STAGE => [
			'head_scripts'   => '',
			'body_scripts'   => '',
			'footer_scripts' => '',
		],
	];

	$options = get_field( 'analytics', 'option' );


	return [
		USSTOVE_ENV_PROD  => wp_parse_args( $options[ USSTOVE_ENV_PROD ], $defaults[ USSTOVE_ENV_PROD ] ),
		USSTOVE_ENV_STAGE => wp_parse_args( $options[ USSTOVE_ENV_STAGE ], $defaults[ USSTOVE_ENV_STAGE ] ),
	];

}
