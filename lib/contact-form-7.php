<?php
/**
 * Filename contact-form-7.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme;

add_filter( 'wpcf7_autop_or_not', '__return_false' );

add_filter( 'wpcf7_load_css', '__return_false' );
