<?php
/**
 * Filename class-drilldown-nav-walker.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme;

/**
 * Class Drilldown_Nav_Walker
 *
 * Summary
 *
 * @package USSC\Theme
 * @author  Peter Toi <peter@petertoi.com>
 * @version
 */
class Drilldown_Nav_Walker extends \Walker_Nav_Menu {
}