<?php
/**
 * Filename navs.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Navs;

/**
 * Prepend items to the offcanvas mobile nav.
 *
 * @see   wp_nav_menu()
 *
 * @param string    $items The HTML list content for the menu items.
 * @param \stdClass $args  An object containing wp_nav_menu() arguments.
 *
 * @return string The modified HTML list content fo the menu items.
 */
function prepend_items_to_nav_mobile( $items, $args ) {
	if ( 'mobile_nav' !== $args->theme_location ) {
		return $items;
	}

	ob_start();
	get_template_part( 'partials/nav-mobile-top' );
	$prepend = ob_get_clean();

	$items = $prepend . "\n" . $items;

	return $items;
}

add_filter( 'wp_nav_menu_items', __NAMESPACE__ . '\\prepend_items_to_nav_mobile', 10, 2 );

/**
 * Add has-mega-menu class to wrapping list item.
 *
 * @param $classes
 * @param $item
 * @param $args
 * @param $depth
 *
 * @return mixed
 */
function add_mega_menu_class( $classes, $item, $args, $depth ) {
	$mega = get_field( 'mega_menu', $item->ID );
	if ( $mega ) {
		$classes[] = 'has-mega-menu';
	}

	return $classes;
}

add_filter( 'nav_menu_css_class', __NAMESPACE__ . '\\add_mega_menu_class', 10, 4 );

/**
 * Add mega menu classes and data attributes to mega menu trigger link.
 *
 * @param $atts
 * @param $item
 * @param $args
 * @param $depth
 *
 * @return mixed
 */
function add_mega_menu_attributes( $atts, $item, $args, $depth ) {
	$mega = get_field( 'mega_menu', $item->ID );
	if ( $mega ) {
		$atts['data-mega-menu-id'] = 'mega-menu-' . $mega;
		$atts['class']             = 'c-mega-menu__trigger js-mega-menu-trigger';
	}

	return $atts;
}

add_filter( 'nav_menu_link_attributes', __NAMESPACE__ . '\\add_mega_menu_attributes', 10, 4 );