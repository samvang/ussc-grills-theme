<?php
/**
 * Filename faqs.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\FAQ;

/**
 * Register FAQ post type.
 */
function register_custom_post_type() {
	/**
	 * FAQ post type labels.
	 */
	$labels = [
		'name'               => esc_html_x( 'FAQs', 'FAQs Post Type General Name', 'ussc' ),
		'singular_name'      => esc_html_x( 'FAQ', 'FAQs Post Type Singular Name', 'ussc' ),
		'add_new'            => esc_html__( 'Add New', 'ussc' ),
		'add_new_item'       => esc_html__( 'Add New FAQ', 'ussc' ),
		'edit_item'          => esc_html__( 'Edit FAQ', 'ussc' ),
		'new_item'           => esc_html__( 'New FAQ', 'ussc' ),
		'view_item'          => esc_html__( 'View FAQ', 'ussc' ),
		'search_items'       => esc_html__( 'Search FAQs', 'ussc' ),
		'not_found'          => esc_html__( 'No FAQs found', 'ussc' ),
		'not_found_in_trash' => esc_html__( 'No FAQs found in Trash', 'ussc' ),
		'parent_item_colon'  => '',
		'all_items'          => esc_html__( 'FAQs', 'ussc' ),
		'menu_name'          => esc_html__( 'FAQs', 'ussc' ),
	];

	/**
	 * FAQ post type supported features.
	 */
	$supports = [ 'title', 'editor' ];

	/**
	 * FAQ post type args
	 */
	$args = [
		'description'         => esc_html__( 'Frequently asked questions.', 'ussc' ),
		'labels'              => $labels,
		'public'              => false,
		'publicly_queryable'  => false,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'query_var'           => true,
		'has_archive'         => true,
		'hierarchical'        => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-admin-comments',
		'supports'            => $supports,
		'can_export'          => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => false,
	];

	register_post_type( 'faq', $args );

	register_taxonomy_for_object_type( 'product_cat', 'faq' );
}

add_action( 'init', __NAMESPACE__ . '\\register_custom_post_type' );

function filter_posts_columns( $columns ) {

	$columns['product_cat'] = __( 'Product Category', 'ussc' );

	return $columns;
}

add_filter( 'manage_faq_posts_columns', __NAMESPACE__ . '\\filter_posts_columns' );

function column_content( $column, $post_id ) {
	if ( 'product_cat' === $column ) {
		$terms = wp_get_post_terms( $post_id, 'product_cat' );
		foreach ( $terms as $term ) {
			echo $term->name . '<br />';
		}
	}

}

add_action( 'manage_faq_posts_custom_column', __NAMESPACE__ . '\\column_content', 10, 2 );
