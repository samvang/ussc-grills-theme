<?php
/**
 * Filename woocommerce.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\WooCommerce;

use USSC\Theme\Assets;


/**
 * Declare WooCommerce Support.
 */
function add_woocommerce_support() {
	add_theme_support( 'woocommerce', array(
		'product_grid' => [
			'default_rows'    => 3,
			'min_rows'        => 2,
			'max_rows'        => 8,
			'default_columns' => 4,
			'min_columns'     => 2,
			'max_columns'     => 5,
		],
	) );
}

add_action( 'after_setup_theme', __NAMESPACE__ . '\\add_woocommerce_support' );

/**
 * Setup Image Sizes.
 */
add_filter( 'woocommerce_get_image_size_single', function ( $size ) {
	return [
		'width'  => 470,
		'height' => 508,
		'crop'   => false,
	];
} );
add_filter( 'woocommerce_get_image_size_thumbnail', function ( $size ) {
	return [
		'width'  => 373,
		'height' => 373,
		'crop'   => false,
	];
} );
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function ( $size ) {
	return [
		'width'  => 70,
		'height' => 75,
		'crop'   => false,
	];
} );

/**
 * Disable WooCommerce Stylesheets - we are using our own.
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );


/**
 * WooCommerce Template Wrapper Start.
 */
function woocommerce_output_content_wrapper() {
	echo '';
}

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
add_action( 'woocommerce_before_main_content', __NAMESPACE__ . '\\woocommerce_output_content_wrapper', 10 );

/**
 * WooCommerce Template Wrapper End.
 */
function woocommerce_output_content_wrapper_end() {
	echo '';
}

remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
add_action( 'woocommerce_after_main_content', __NAMESPACE__ . '\\woocommerce_output_content_wrapper_end', 10 );

/**
 * Disable Page Title.
 */
//add_filter( 'woocommerce_show_page_title', '__return_false' );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );


/**
 * Add our BEM classes to individual products.
 *
 * @param   array   $classes  Classes to filter.
 * @param   string  $class    Other class to filter.
 * @param   int     $post_id  Post ID.
 *
 * @return array Modified classes.
 */
function product_classes( $classes, $class, $post_id ) {
	$post = get_post( $post_id );

	if ( 'product' === $post->post_type ) {
		$classes[] = 'c-product';
	}

	return $classes;
}

add_filter( 'post_class', __NAMESPACE__ . '\\product_classes', 10, 3 );

/**
 * Breadcrumbs.
 *
 * @see woocommerce_breadcrumb()
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

/**
 * Disable before_shop_loop features.
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * Disable Sale flashes.
 *
 * @see woocommerce_show_product_loop_sale_flash()
 * @see woocommerce_show_product_sale_flash()
 */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

/**
 * Product Summary Box.
 *
 * @see woocommerce_template_single_title()
 * @see woocommerce_template_single_rating()
 * @see woocommerce_template_single_price()
 * @see woocommerce_template_single_excerpt()
 * @see woocommerce_template_single_meta()
 * @see woocommerce_template_single_sharing()
 */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

/**
 * Load Product_Cat meta template for designating products within this category
 * and child categories as being available only through resellers, and not from this website.
 */
function product_cat_add_form_fields() {
	get_template_part( 'partials/admin/product_cat-meta-reseller-only-add' );
	get_template_part( 'partials/admin/product_cat-meta-hide-price-add' );
	get_template_part( 'partials/admin/product_cat-meta-display-msrp-on-price-add' );
}

add_action( 'product_cat_add_form_fields', __NAMESPACE__ . '\\product_cat_add_form_fields' );

/**
 * Load Product_Cat meta template for designating products within this category
 * and child categories as being available only through resellers, and not from this website.
 */
function product_cat_edit_form_fields() {
	get_template_part( 'partials/admin/product_cat-meta-reseller-only-edit' );
	get_template_part( 'partials/admin/product_cat-meta-hide-price-edit' );
	get_template_part( 'partials/admin/product_cat-meta-display-msrp-on-price-edit' );
}

add_action( 'product_cat_edit_form_fields', __NAMESPACE__ . '\\product_cat_edit_form_fields' );

/**
 * Save Product_Cat meta for designating products as available only through resellers.
 */
function save_product_cat_meta( $term_id, $tt_id, $taxonomy ) {
	if ( 'product_cat' !== $taxonomy ) {
		return;
	}

	$reseller_only = filter_input( INPUT_POST, 'ussc_reseller_only', FILTER_SANITIZE_STRING );
	update_term_meta( $term_id, 'ussc_reseller_only', ( 'on' === $reseller_only ) );

	$hide_price = filter_input( INPUT_POST, 'ussc_hide_price', FILTER_SANITIZE_STRING );
	update_term_meta( $term_id, 'ussc_hide_price', ( 'on' === $hide_price ) );

	$display_msrp_on_price = filter_input( INPUT_POST, 'ussc_display_msrp_on_price', FILTER_SANITIZE_STRING );
	update_term_meta( $term_id, 'ussc_display_msrp_on_price', ( 'on' === $display_msrp_on_price ) );
}

add_action( 'created_term', __NAMESPACE__ . '\\save_product_cat_meta', 10, 3 );
add_action( 'edit_term', __NAMESPACE__ . '\\save_product_cat_meta', 10, 3 );

/**
 * Add column to see if category is reseller-only.
 *
 * @param $columns
 *
 * @return array
 */
function manage_product_cat_reseller_column( $columns ) {

	$i = array_search( 'slug', array_keys( $columns ), true );

	$new_col = [ 'reseller_only' => __( 'Reseller Only', 'ussc' ) ];

	$columns = array_slice( $columns, 0, $i ) + $new_col + array_slice( $columns, 1 );

	return $columns;
}

add_filter( 'manage_edit-product_cat_columns', __NAMESPACE__ . '\\manage_product_cat_reseller_column' );

/**
 * Populate reseller-only column.
 *
 * @param $content
 * @param $column_name
 * @param $term_id
 *
 * @return string
 */
function custom_product_cat_reseller_column( $content, $column_name, $term_id ) {

	if ( 'reseller_only' === $column_name ) {
		$reseller_only = get_term_meta( $term_id, 'ussc_reseller_only', true );
		if ( empty( $reseller_only ) ) {
			$content .= '<span aria-hidden="true">—</span>';
		} else {
			$content .= '<span class="dashicons dashicons-yes"></span>';
		}
	}

	return $content;
}

add_action( 'manage_product_cat_custom_column', __NAMESPACE__ . '\\custom_product_cat_reseller_column', 10, 3 );

/**
 * Replace Add To Cart button with Find a Reseller.
 */
function find_a_retailer_button() {
	global $post;

	$reseller_only = false;

	$product_cats = wp_get_post_terms( $post->ID, 'product_cat' );

	foreach ( $product_cats as $cat ) {
		$cat_reseller_only = get_term_meta( $cat->term_id, 'ussc_reseller_only', true );
		if ( $cat_reseller_only ) {
			$reseller_only = true;
			break;
		}
	}

	if ( $reseller_only ) {
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
		add_action( 'woocommerce_single_product_summary', __NAMESPACE__ . '\\woocommerce_template_single_find_a_reseller', 30 );
	}
}

//add_action( 'woocommerce_single_product_summary', __NAMESPACE__ . '\\find_a_retailer_button', 1 );

/**
 * Render Find a Reseller button.
 */
function woocommerce_template_single_find_a_reseller() {
	printf( '<a href="#" class="button primary">%s</a>',
		esc_html_x( 'Find a Reseller', 'ussc' )
	);
}

/**
 * Change the placeholder image
 */
function woocommerce_placeholder_img_src( $src ) {
	$src = Assets\asset_path( 'images/product-placeholder.png' );

	return $src;
}

add_filter( 'woocommerce_placeholder_img_src', __NAMESPACE__ . '\\woocommerce_placeholder_img_src' );

/**
 * Filter WooCommerce Product price to add Suggested MSRP on Reseller Only products.
 *
 * @param   string       $price
 * @param   \WC_Product  $product
 *
 * @return string
 */
function woocommerce_get_price_html( $price, $product ) {
	if ( is_admin() ) {
		return $price;
	}

	$hide_price   = false;
	$display_msrp = false;

	$product_cat_ids = wc_get_product_term_ids( $product->get_id(), 'product_cat' );
	foreach ( $product_cat_ids as $product_cat_id ) {
		if ( get_term_meta( $product_cat_id, 'ussc_hide_price', true ) ) {
			$hide_price = true;
		}
		if ( get_term_meta( $product_cat_id, 'ussc_display_msrp_on_price', true ) ) {
			$display_msrp = true;
		}
	}

	if ( $hide_price ) {
		$price = '';
	}

	if ( $display_msrp ) {
		$price = sprintf( '%s<span class="price__msrp">%s</span>',
			$price,
			__( 'Suggested MSRP', 'ussc' )
		);
	}

	return $price;
}

add_filter( 'woocommerce_get_price_html', __NAMESPACE__ . '\\woocommerce_get_price_html', 10, 2 );

/**
 * Fix for issue introduced in 3.5.1 where wc_checkout_fields_uasort_comparison()
 * applies uasort without checking if priority exists in field array.
 *
 * @param   array  $field_groups  The original field groups.
 *
 * @return array Field groups with priority.
 */
function add_priority_to_woocommerce_checkout_fields( $field_groups ) {
	$prioritized_field_groups = [];
	foreach ( $field_groups as $key => $field_group ) {
		$prioritized_field_group = array_map( function ( $field ) {
			if ( ! isset( $field['priority'] ) ) {
				$field['priority'] = 10;
			}

			return $field;
		}, $field_group );

		$prioritized_field_groups[ $key ] = $prioritized_field_group;
	}

	return $prioritized_field_groups;
}

add_filter( 'woocommerce_checkout_fields', __NAMESPACE__ . '\\add_priority_to_woocommerce_checkout_fields' );

/**
 * Set default country on the checkout for non-existing users only to US
 *
 * @param $country
 *
 * @return string
 */
function change_default_checkout_country( $country ) {
	// If the user already exists, don't override country.
	if ( WC()->customer->get_is_paying_customer() ) {
		return $country;
	}

	return 'US';
}

add_filter( 'default_checkout_billing_country', __NAMESPACE__ . '\\change_default_checkout_country', 10, 1 );

/**
 * Show cart contents / total Ajax.
 *
 * @param $fragments
 *
 * @return mixed
 */
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	// TODO: Move to partial.
	?>
	<a class="cart-customlocation" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'woothemes' ); ?>">
		<?php echo sprintf( _n( '%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes' ), $woocommerce->cart->cart_contents_count ); ?> - <?php echo $woocommerce->cart->get_cart_total(); ?>
	</a>
	<?php
	$fragments['a.cart-customlocation'] = ob_get_clean();

	return $fragments;
}

add_filter( 'woocommerce_add_to_cart_fragments', __NAMESPACE__ . '\\woocommerce_header_add_to_cart_fragment' );

/**
 * Auto Complete all WooCommerce orders.
 *
 * @param   int  $order_id  The Order ID.
 */
function auto_complete_order( $order_id ) {
	if ( ! $order_id ) {
		return;
	}

	$order = wc_get_order( $order_id );
	$order->update_status( 'completed' );
}

add_action( 'woocommerce_thankyou', __NAMESPACE__ . '\\auto_complete_order' );

/**
 * Output dataLayer object for Order Confirmation page.
 */
function data_layer_order_confirmation() {
	if ( ! is_order_received_page() ) {
		return false;
	}

	global $wp_query;
	$order_id = $wp_query->get( 'order-received' );

	$order = wc_get_order( $order_id );

	$order_arr = [
		'transactionId'       => "{$order->get_id()}",
		'transactionTotal'    => floatval( $order->get_total() ),
		'transactionShipping' => floatval( $order->get_shipping_total() ),
	];

	$products    = [];
	$order_items = $order->get_items();
	foreach ( $order_items as $order_item ) {
		$product    = $order_item->get_product();
		$products[] = [
			'sku'      => "{$product->get_sku()}",
			'name'     => "{$product->get_name()}",
			'price'    => floatval( $product->get_price() ),
			'quantity' => intval( $order_item->get_quantity() ),
		];
	}

	$order_arr['transactionProducts'] = $products;

	$order_obj = wp_json_encode( $order_arr );

	$data_layer = "window.dataLayer = window.dataLayer || []; dataLayer.push({$order_obj});";

	echo "<script type='text/javascript'>$data_layer</script>"; //phpcs:ignore
}

add_action( 'usstove_data_layer_head', __NAMESPACE__ . '\\data_layer_order_confirmation' );

function data_layer_where_to_buy_retailer() {
	if ( ! is_product() ) {
		return false;
	}

	$product = wc_get_product();

	$product_data = [
		'name'  => $product->get_name(),
		'price' => floatval( $product->get_price() ),
	];

	$data_layer = <<<JS
	(
		function($, undefined) {
			$('#where-to-buy a').on('click', function(e) {
				dataLayer.push({
					'event': 'Retailer Click',
					'retailerName': $(this).data('retailer-name'),
					'productName': '{$product_data['name']}',
					'productValue': {$product_data['price']}
					});
			});
		}
	)(jQuery);
JS;

	echo "<script type='text/javascript'>$data_layer</script>"; //phpcs:ignore

}

add_action( 'usstove_data_layer_footer', __NAMESPACE__ . '\\data_layer_where_to_buy_retailer' );

// hide coupon field on cart page
function hide_coupon_field_on_cart( $enabled ) {
	if ( is_cart() ) {
		$enabled = false;
	}

	return $enabled;
}

add_filter( 'woocommerce_coupons_enabled', __NAMESPACE__ . '\\hide_coupon_field_on_cart' );
