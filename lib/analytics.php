<?php
/**
 * Filename analytics.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

namespace USSC\Theme\Analytics;

use USSC\Theme\Options;

/**
 * Output analytics scripts in <head>
 */
function print_head_scripts() {
	echo '<!-- head scripts -->';
	do_action( 'usstove_data_layer_head' );
	$scripts = Options\get_analytics_options();
	echo $scripts[ USSTOVE_ENV ]['head_scripts']; //phpcs:ignore
}

add_action( 'wp_head', __NAMESPACE__ . '\\print_head_scripts' );

/**
 * Output analytics scripts just after opening <body> tag
 */
function print_body_scripts() {
	echo '<!-- body scripts -->';
	$scripts = Options\get_analytics_options();
	echo $scripts[ USSTOVE_ENV ]['body_scripts']; //phpcs:ignore
}

add_action( 'get_header', __NAMESPACE__ . '\\print_body_scripts' );

/**
 * Output analytics scripts just before closing </body> tag
 */
function print_footer_scripts() {
	echo '<!-- footer scripts -->';
	do_action( 'usstove_data_layer_footer' );
	$scripts = Options\get_analytics_options();
	echo $scripts[ USSTOVE_ENV ]['footer_scripts']; //phpcs:ignore
}

add_action( 'wp_print_footer_scripts', __NAMESPACE__ . '\\print_footer_scripts', 999 );


