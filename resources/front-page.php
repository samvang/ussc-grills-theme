<?php
/**
 * Filename front-page.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>

<?php get_template_part( 'sections/carousel-home' ); ?>

<?php get_template_part( 'sections/featured-products' ); ?>

<?php get_template_part( 'sections/featured-product' ); ?>
