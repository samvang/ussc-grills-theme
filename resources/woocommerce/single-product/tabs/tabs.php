<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

use Edgenet\Post_Types\Document;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php
$product_cat_ids = wc_get_product_cat_ids( get_the_ID() );

$faqs = get_posts( [
	'post_type' => 'faq',
	'tax_query' => [ // phpcs:ignore
		[
			'taxonomy' => 'product_cat',
			'terms'    => $product_cat_ids,
		],
	],
] );
?>
<div class="product__tabs">
	<div class="c-tabs--product">

		<ul class="tabs" id="Product_Sections" role="tablist" data-responsive-accordion-tabs="accordion large-tabs" data-allow-all-closed="true">
			<li class="tabs-title is-active tabs__title" id="tab-title-details" role="tab" aria-controls="tab-details">
				<a class="tabs__tab is-active" href="#tab-details"><?php esc_html_e( 'Details', 'ussc' ); ?></a>
			</li>
			<?php
			// TODO: Relate Products & Parts before enabling this feature.
			/**
			 * <li class="tabs-title tabs__title" id="tab-title-parts" role="tab" aria-controls="tab-products">
			 * <a class="tabs__tab" href="#tab-products"><?php esc_html_e( 'Parts', 'ussc' ); ?></a>
			 * </li>
			 * <li class="tabs-title tabs__title" id="tab-title-products" role="tab" aria-controls="tab-products">
			 * <a class="tabs__tab" href="#tab-products"><?php esc_html_e( 'Products', 'ussc' ); ?></a>
			 * </li>
			 */
			?>
			<li class="tabs-title tabs__title" id="tab-title-downloads" role="tab" aria-controls="tab-downloads">
				<a class="tabs__tab" href="#tab-downloads"><?php esc_html_e( 'Manuals & Downloads', 'ussc' ); ?></a>
			</li>
			<li class="tabs-title tabs__title hide-for-large" id="tab-title-faqs" role="tab" aria-controls="tab-faqs">
				<a class="tabs__tab" href="#tab-faqs"><?php esc_html_e( 'FAQs', 'ussc' ); ?></a>
			</li>
		</ul>

		<div class="tabs-content tabs__content" data-tabs-content="Product_Sections">
			<div class="tabs-panel is-active tabs__panel entry-content" id="tab-details" role="tabpanel" aria-labelledby="tab-title-details">
				<div class="row">
					<div class="column large-8">

						<ul class="accordion" data-accordion data-allow-all-closed="true">

							<?php $features = get_field( 'ussc_features' ); ?>
							<?php if ( $features ) : ?>
								<li class="accordion-item is-active product__section" data-accordion-item>
									<a href="#" class="accordion-title section__title"><?php esc_html_e( 'Features', 'ussc' ); ?></a>
									<div class="accordion-content section__content" data-tab-content>
										<ul class="content__feature-list">
											<?php foreach ( $features as $feature ) : ?>
												<li><?php echo esc_html( $feature['feature'] ); ?></li>
											<?php endforeach; ?>
										</ul>
									</div>
								</li>
							<?php endif; ?>

							<?php $product = wc_get_product( get_the_ID() ); ?>
							<?php $atts = $product->get_attributes(); ?>
							<?php if ( $atts ) : ?>
								<li class="accordion-item product__section" data-accordion-item>
									<a href="#" class="accordion-title section__title"><?php esc_html_e( 'Specifications', 'ussc' ); ?></a>
									<div class="accordion-content section__content" data-tab-content>
										<div class="content__table">
											<table>
												<?php foreach ( $atts as $tax => $att ) : ?>
													<?php $taxonomy = get_taxonomy( $tax ); ?>
													<tr>
														<th><?php echo esc_html( $taxonomy->label ); ?></th>
														<td>
															<?php
															$terms = [];
															foreach ( $att->get_options() as $option ) {
																$terms[] = get_term( $option, $tax )->name;
															}
															echo implode( ', ', $terms );
															?>
														</td>
													</tr>
												<?php endforeach; ?>
											</table>
										</div>
									</div>
								</li>
							<?php endif; ?>

							<?php $dimensions = get_field('ussc_dimensions'); ?>
							<?php if ( $dimensions ) : ?>
								<li class="accordion-item product__section" data-accordion-item>
									<a href="#" class="accordion-title section__title"><?php esc_html_e( 'Dimensions', 'ussc' ); ?></a>
									<div class="accordion-content section__content" data-tab-content>
										<div class="content__table">
											<table>
												<?php foreach ( $dimensions as $dimension ) : ?>
													<tr>
														<th><?php echo esc_html( $dimension['label'] ); ?></th>
														<td><?php echo esc_html( $dimension['value'] ); ?></td>
													</tr>
												<?php endforeach; ?>
											</table>
										</div>
									</div>
								</li>
							<?php endif; ?>

							<?php $others = get_field('ussc_other'); ?>
							<?php if ( $others ) : ?>
								<li class="accordion-item product__section" data-accordion-item>
									<a href="#" class="accordion-title section__title"><?php esc_html_e( 'Other', 'ussc' ); ?></a>
									<div class="accordion-content section__content" data-tab-content>
										<div class="content__table">
											<table>
												<?php foreach ( $others as $other ) : ?>
													<tr>
														<th><?php echo esc_html( $other['label'] ); ?></th>
														<td><?php echo esc_html( $other['value'] ); ?></td>
													</tr>
												<?php endforeach; ?>
											</table>
										</div>
									</div>
								</li>
							<?php endif; ?>

							<?php $regulatorys = get_field('ussc_regulatory'); ?>
							<?php if ( $regulatorys ) : ?>
								<li class="accordion-item product__section" data-accordion-item>
									<a href="#" class="accordion-title section__title"><?php esc_html_e( 'Regulatory', 'ussc' ); ?></a>
									<div class="accordion-content section__content" data-tab-content>
										<div class="content__table">
											<table>
												<?php foreach ( $regulatorys as $regulatory ) : ?>
													<tr>
														<th><?php echo esc_html( $regulatory['label'] ); ?></th>
														<td><?php echo esc_html( $regulatory['value'] ); ?></td>
													</tr>
												<?php endforeach; ?>
											</table>
										</div>
									</div>
								</li>
							<?php endif; ?>

						</ul>
					</div>
					<div class="column large-4 show-for-large">
						<ul class="accordion" data-accordion data-allow-all-closed="true">
							<li class="accordion-item product__section is-active" data-accordion-item>
								<a href="#" class="accordion-title section__title"><?php esc_html_e( 'FAQs', 'ussc' ); ?></a>
								<div class="accordion-content section__content" data-tab-content>
									<div class="content__faqs">
										<ul class="accordion" data-accordion>
											<?php foreach ( $faqs as $faq ) : ?>
												<li class="accordion-item faqs__faq" data-accordion-item>
													<!-- Accordion tab title -->
													<a href="#" class="accordion-title faq__title"><?php echo $faq->post_title; ?></a>

													<!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
													<div class="accordion-content faq__content" data-tab-content>
														<?php echo $faq->post_content; ?>
													</div>
												</li>
											<?php endforeach; ?>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<?php
			// TODO: Relate Products & Parts before enabling this feature.
			/**
			 * <div class="tabs-panel tabs__panel entry-content" id="tab-parts" role="tabpanel" aria-labelledby="tab-title-parts">
			 * <div class="column large-12">
			 * Parts
			 * </div>
			 * </div>
			 * <div class="tabs-panel tabs__panel entry-content" id="tab-products" role="tabpanel" aria-labelledby="tab-title-products">
			 * <div class="column large-12">
			 * Products
			 * </div>
			 * </div>
			 */
			?>
			<div class="tabs-panel tabs__panel entry-content product__section" id="tab-downloads" role="tabpanel" aria-labelledby="tab-title-downloads">
				<div class="row">
					<div class="column small-12 section__content">
						<?php
						$documents = get_field('related_documents');
						?>
						<table class="c-document-table stack">
							<thead>
							<tr>
								<th><?php esc_html_e( 'Document', 'usstove' ); ?></th>
								<th><?php esc_html_e( 'File Type', 'usstove' ); ?></th>
								<th><?php esc_html_e( 'File Size', 'usstove' ); ?></th>
								<th><span class="sr-only"><?php esc_html_e( 'Product', 'usstove' ); ?></span></th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ( $documents as $document ) : ?>
								<?php //$attachment_id = get_post_meta( $document->ID, Document::META_ATTACHMENT_ID, true ); ?>
								<?php $attachment_id = get_field( $document->ID, 'document'); ?>
								<tr>
									<td>
										<a href="<?php echo esc_attr( wp_get_attachment_url( $attachment_id ) ); ?>" target="_blank">
											<span class="doc__title"><?php echo esc_html( $document->post_title ); ?></span>
										</a>
									</td>
									<td>
										<span class="doc__type"><?php echo esc_html( get_post_mime_type( $attachment_id ) ); ?></span>
									</td>
									<td>
										<span class="doc__size"><?php echo esc_html( size_format( filesize( get_attached_file( $attachment_id ) ) ) ); ?></span>
									</td>
									<td>
										<a href="<?php echo esc_attr( wp_get_attachment_url( $attachment_id ) ); ?>" target="_blank">
											<span class="doc__download"><?php esc_html_e( 'Download', 'usstove' ); ?></span>️
										</a>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="tabs-panel tabs__panel entry-content product__section hide-for-large" id="tab-faqs" role="tabpanel" aria-labelledby="tab-title-downloads">
				<div class="column large-8 section__content">
					<div class="content__faqs">
						<ul class="accordion" data-accordion>
							<?php foreach ( $faqs as $faq ) : ?>
								<li class="accordion-item faqs__faq" data-accordion-item>
									<!-- Accordion tab title -->
									<a href="#" class="accordion-title faq__title"><?php echo esc_html( $faq->post_title ); ?></a>

									<!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
									<div class="accordion-content faq__content" data-tab-content>
										<?php echo wp_kses_post( $faq->post_content ); ?>
									</div>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
