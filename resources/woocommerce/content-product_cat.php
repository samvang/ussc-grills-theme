<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<li <?php wc_product_cat_class( 'column', $category ); ?>>
	<a href="<?php echo esc_attr( get_term_link( $category, 'product_cat' ) ); ?>">
		<div class="c-card--cat">
			<div class="c-card__thumb">
				<?php woocommerce_subcategory_thumbnail( $category ); ?>
				<div class="thumb__overlay">
					<button class="button hollow white">
						<?php esc_html_e( 'View Products', 'ussc' ); ?>
					</button>
				</div>
			</div>
			<div class="c-card__content">
				<h2 class="content__title"><?php echo wp_kses_post( $category->name ); ?></h2>
			</div>
		</div>
	</a>
</li>
