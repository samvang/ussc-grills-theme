<?php
/**
 * Filename template-brands.php
 *
 * Template Name: Brand Page Template
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use USSC\Theme\Helpers;
use Edgenet\Taxonomies\Brand;

?>
<?php while ( have_posts() ) : ?>
	<div class="l-intro">
		<?php the_post(); ?>
		<div class="intro__head">
			<?php get_template_part( 'partials/page', 'header' ); ?>
		</div>
		<?php if ( ! empty( get_the_content() ) ) : ?>
			<div class="intro__copy">
				<?php get_template_part( 'partials/content', 'page' ); ?>
			</div>
		<?php endif; ?>
	</div>
	<?php
	$brands = get_terms( [
		'taxonomy' => Brand::TAXONOMY,
	] );
	?>
	<?php if ( $brands ) : ?>
		<div class="l-card-grid--brands align-center">
			<?php foreach ( $brands as $brand ) : ?>
				<div class="column">
					<div class="c-card--brand">
						<a href="<?php echo esc_attr( get_term_link( $brand, Brand::TAXONOMY ) ); ?>">
							<div class="c-card__thumb">
								<?php echo wp_kses_post( Helpers\get_brand_tile_thumbnail( $brand, 'card-thumb-page' ) ); ?>
								<div class="thumb__overlay">
									<button class="button hollow white"><?php esc_html_e( 'View Products', 'ussc' ); ?></button>
								</div>
							</div>
							<div class="c-card__content">
								<h3 class="content__title">
									<?php echo esc_html( $brand->name ); ?>
								</h3>
							</div>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
<?php endwhile; ?>
