<?php
/**
 * Filename template-documents.php
 * Template Name: Document Library Template
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use Edgenet\Post_Types\Document;
use Edgenet\Taxonomies\Doc_Type;

?>
<?php while ( have_posts() ) : ?>
	<div class="l-intro">
		<?php the_post(); ?>
		<div class="intro__head">
			<?php get_template_part( 'partials/page', 'header' ); ?>
		</div>
		<?php if ( ! empty( get_the_content() ) ) : ?>
			<div class="intro__copy">
				<?php get_template_part( 'partials/content', 'page' ); ?>
			</div>
		<?php endif; ?>
	</div>

	<?php
	$doc_types = get_field( 'doc_types' );

	if ( ! empty( $doc_types ) ) {
		$doc_args  = [
			'post_type'      => Document::POST_TYPE,
			'orderby'        => [ 'post_title' => 'ASC' ],
			'posts_per_page' => - 1,
			'tax_query'      => [ // phpcs:ignore
				[
					'taxonomy' => Doc_Type::TAXONOMY,
					'terms'    => array_map( function ( $term ) {
						return $term->term_id;
					}, $doc_types ),
				],
			],
		];
		$documents = get_posts( $doc_args );
	} else {
		$documents = false;
	}
	?>

	<?php if ( $documents ) : ?>
		<div class="row">
			<table class="c-document-table stack">
				<thead>
				<tr>
					<th><?php esc_html_e( 'Document', 'usstove' ); ?></th>
					<th><?php esc_html_e( 'File Type', 'usstove' ); ?></th>
					<th><?php esc_html_e( 'File Size', 'usstove' ); ?></th>
					<th><span class="sr-only"><?php esc_html_e( 'Product', 'usstove' ); ?></span></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ( $documents as $document ) : ?>
					<?php //$attachment_id = get_post_meta( $document->ID, Document::META_ATTACHMENT_ID, true ); ?>
					<?php $attachment_id = get_field( $document->ID, 'document'); ?>
					<tr>
						<td>
							<a href="<?php echo esc_attr( wp_get_attachment_url( $attachment_id ) ); ?>" target="_blank">
								<span class="doc__title"><?php echo esc_html( $document->post_title ); ?></span>
							</a>
						</td>
						<td>
							<span class="doc__type"><?php echo esc_html( get_post_mime_type( $attachment_id ) ); ?></span>
						</td>
						<td>
							<span class="doc__size"><?php echo esc_html( size_format( filesize( get_attached_file( $attachment_id ) ) ) ); ?></span>
						</td>
						<td>
							<a href="<?php echo esc_attr( wp_get_attachment_url( $attachment_id ) ); ?>" target="_blank">
								<span class="doc__download"><?php esc_html_e( 'Download', 'usstove' ); ?></span>️
							</a>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	<?php endif; ?>
<?php
endwhile;

