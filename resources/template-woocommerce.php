<?php
/**
 * Filename template-woocommerce.php
 *
 * Template Name: WooCommerce Cart/Checkout
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>

<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
	<div class="l-intro">
		<div class="intro__head">
			<?php get_template_part( 'partials/page', 'header' ); ?>
		</div>
	</div>
	<div class="row">
		<div class="page-content">
			<?php get_template_part( 'partials/content', 'page' ); ?>
		</div>
	</div>
<?php endwhile;
