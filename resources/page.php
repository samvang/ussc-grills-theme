<?php
/**
 * Filename page.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$hero = get_field( 'hero' );
?>

<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>

	<?php if ( ! empty( $hero['background_image'] ) ) : ?>
		<div class="row--wide column">
			<section class="c-page-hero" style="background-image: url(<?php echo esc_attr( wp_get_attachment_image_url( $hero['background_image'], 'full' ) ); ?>)">
				<div class="slide__overlay"></div>
				<?php if ( ! empty( $hero['title'] ) ) : ?>
					<div class="row">
						<div class="column large-7">
							<div class="hero__heading">
								<h1 class="title"><?php echo $hero['title']; ?></h1>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</section>
		</div>
	<?php endif; ?>

	<div class="row">
		<div class="column">
			<?php if ( empty( $hero['background_image'] ) && empty( $hero['title'] ) ) : ?>
				<?php get_template_part( 'partials/page', 'header' ); ?>
			<?php endif; ?>
			<div class="page-content">
				<?php get_template_part( 'partials/content', 'page' ); ?>
			</div>
		</div>
	</div>
<?php endwhile;
