<?php
/**
 * Filename template-support.php
 *
 * Template Name: Support Page Template
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use USSC\Theme\Helpers;

?>
<?php while ( have_posts() ) : ?>
	<div class="l-intro">
		<?php the_post(); ?>
		<div class="intro__head">
			<?php get_template_part( 'partials/page', 'header' ); ?>
		</div>
		<?php if ( ! empty( get_the_content() ) ) : ?>
			<div class="intro__copy">
				<?php get_template_part( 'partials/content', 'page' ); ?>
			</div>
		<?php endif; ?>
	</div>
	<?php $child_pages = get_pages( [ 'parent' => get_the_ID() ] ); ?>
	<?php if ( $child_pages ) : ?>
		<div class="l-card-grid">
			<?php foreach ( $child_pages as $child_page ) : ?>
				<div class="column">
					<div class="c-card--page">
						<a href="<?php echo esc_attr( get_permalink( $child_page->ID ) ); ?>">
							<div class="c-card__thumb">
								<?php echo wp_kses_post( Helpers\get_page_tile_thumbnail( $child_page->ID, 'card-thumb-page' ) ); ?>
								<div class="thumb__overlay">
									<button class="button hollow white"><?php esc_html_e( 'View Page', 'ussc' ); ?></button>
								</div>
							</div>
							<div class="c-card__content">
								<h3 class="content__title">
									<?php echo esc_html( $child_page->post_title ); ?>
								</h3>
							</div>
						</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
<?php endwhile;
