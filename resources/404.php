<?php
/**
 * Filename 404.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$fields = get_field( 'error_page', 'option' );
?>

<section class="c-carousel--error">
	<div class="row--wide column">
		<ul class="c-carousel__container">
			<li
					class="c-carousel__slide"
					style="background-image: url(<?php echo wp_get_attachment_image_url( $fields['banner_image']['ID'], 'full' ); //phpcs:ignore ?>)"
			>
				<div class="column small-12 medium-10 medium-offset-1">
					<h2 class="title">
						<?php echo wp_kses_post( $fields['title'] ); ?>
					</h2>
				</div>
				<div class="column small-12 medium-6 medium-offset-3">
					<div class="search">
						<?php get_product_search_form(); ?>
					</div>
				</div>
			</li>
		</ul>
	</div>
</section>

<div class="row">
	<div class="column small-12 medium-10 medium-offset-1">
		<div class="page-content">
			<?php echo wp_kses_post( $fields['content'] ); ?>
		</div>
	</div>
</div>
