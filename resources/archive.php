<?php
/**
 * Filename archive.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>

	<div class="l-intro">
		<div class="intro__head">
			<?php get_template_part( 'partials/page', 'header' ); ?>
		</div>
		<?php if ( ! empty( term_description() ) ) : ?>
			<div class="intro__copy">
				<?php echo term_description(); ?>
			</div>
		<?php endif; ?>
	</div>
	<div class="l-card-grid">
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<div class="column">
				<?php get_template_part( 'partials/content', 'post' ); ?>
			</div>
		<?php endwhile; ?>
	</div>

<?php
the_posts_navigation();
