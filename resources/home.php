<?php
/**
 * Filename home.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$page_for_posts_id = get_option( 'page_for_posts' );
$page_for_posts    = get_post( $page_for_posts_id );
$hero              = get_field( 'hero', $page_for_posts_id );
?>

<?php if ( ! empty( $hero['background_image'] ) ) : ?>
	<div class="row--wide column">
		<section class="c-page-hero" style="background-image: url(<?php echo esc_attr( wp_get_attachment_image_url( $hero['background_image'], 'full' ) ); ?>)">
			<div class="slide__overlay"></div>
			<?php if ( ! empty( $hero['title'] ) ) : ?>
				<div class="row">
					<div class="column large-7">
						<div class="hero__heading">
							<h1 class="title"><?php echo $hero['title']; ?></h1>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</section>
	</div>
<?php endif; ?>

<div class="row">
	<div class="column small-12">
		<?php if ( empty( $hero['background_image'] ) && empty( $hero['title'] ) ) : ?>
			<?php get_template_part( 'partials/page', 'header' ); ?>
		<?php endif; ?>
		<div class="page-content">
			<?php echo apply_filters( 'the_content', $page_for_posts->post_content ); ?>
		</div>
	</div>
</div>

<div class="l-card-grid--3">
	<?php while ( have_posts() ) : ?>
		<?php the_post(); ?>
		<div class="column">
			<?php get_template_part( 'partials/content', 'post' ); ?>
		</div>
	<?php endwhile; ?>
</div>

<section class="prev-next">
	<div class="row column">
		<?php the_posts_navigation(); ?>
	</div>
</section>
