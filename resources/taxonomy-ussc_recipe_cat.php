<?php
/**
 * Filename category.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use USSC\Theme\Category;

$the_category = get_queried_object();

$featured_posts     = Category\get_featured_posts( $the_category );
$featured_posts_ids = array_map( function ( $featured_post ) {
	return $featured_post->ID;
}, $featured_posts );

$ppp = get_option( 'posts_per_page' );

$count = 0;

?>
	<section class="c-carousel--recipes">
		<div class="row--wide column">
			<ul class="c-carousel__container js-carousel-recipes">
				<?php foreach ( $featured_posts as $featured_post ) : ?>
					<li
						class="c-carousel__slide"
						style="background-image: url(<?php echo esc_attr( get_the_post_thumbnail_url( $featured_post, 'full' ) ); ?>)"
					>
						<div class="row">
							<div class="column small-12 medium-5">
								<h2 class="title"><?php echo esc_html( $featured_post->post_title ); ?></h2>
								<a href="<?php echo esc_attr( get_the_permalink( $featured_post->ID ) ); ?>" class="button">
									<?php esc_html_e( 'View Recipe', 'usstove' ); ?>
								</a>
							</div>
						</div>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	</section>

<?php
$categories = get_terms( [
	'taxonomy' => 'ussc_recipe_cat',
] );
?>
	<section class="c-category-nav">
		<div class="row small-up-1 medium-up-3">
			<?php foreach ( $categories as $category ) : ?>
				<div class="column">
					<div class="nav-item align-middle align-center">
						<?php printf( '<a class="nav-link" href="%s">%s</a>', esc_attr( get_category_link( $category ) ), esc_html( $category->name ) ); ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>

	<header>
		<div class="row column">
			<h1 class="text-center"><?php printf( _x( '%s Recipes', '', '' ), esc_html( $the_category->name ) ); ?></h1>
		</div>
	</header>

	<div class="l-card-grid--3">
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<div class="column">
				<?php get_template_part( 'partials/content', 'post' ); ?>
			</div>
		<?php endwhile; ?>
	</div>

<?php the_posts_navigation();
