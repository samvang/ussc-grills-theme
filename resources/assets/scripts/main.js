// External Dependencies.
import $ from 'jquery';

// Module_Loader
import ModuleLoader from './utils/ModuleLoader';

// Modules
import foundation from './modules/foundation';
import carousels from './modules/carousels';
import mega_menu from './modules/mega_menu';
import off_canvas from './modules/off_canvas';
import pencil_ad from './modules/pencil_ad';
import tabs from './modules/tabs';
import form from './modules/form';

const modules = new ModuleLoader( {
	foundation,
	carousels,
	mega_menu,
	off_canvas,
	pencil_ad,
	tabs,
	form,
} );

$( document ).ready( () => modules.init() );
