import 'jquery';
import 'select2/dist/js/select2.min';

export default {
  init() {
    $('select').select2({
      theme: 'ussc',
    });
  },
};