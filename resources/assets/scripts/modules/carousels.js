import $ from 'jquery';
import { MediaQuery } from 'foundation-sites/js/foundation.util.mediaQuery';
import 'slick-carousel/slick/slick.min';

export default {
	init() {
		this.initHomeCarousel();
		this.initRecipeCarousel();
		this.initFeaturedCategoryCarousel();
		this.initFeaturedProductCarousel();
		this.initFeaturedRecipeCarousel();
		this.initProductCarousels();
	},
	initHomeCarousel() {
		const $home = $( '.js-carousel-home' );

		if ( 0 === $home.length ) {
			return;
		}

		$home.slick();
	},
	initRecipeCarousel() {
		const $recipes = $( '.js-carousel-recipes' );

		if ( 0 === $recipes.length ) {
			return;
		}

		$recipes.slick();
	},
	initFeaturedCategoryCarousel() {
		const $feat_cats = $( '.js-carousel-cats' );

		if ( 0 === $feat_cats.length ) {
			return;
		}

		$feat_cats.slick( {
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: true,
			responsive: [
				{
					breakpoint: 1170,
					settings: {
						arrows: false,
						centerMode: true,
						dots: true,
						slidesToShow: 3,
					},
				},
				{
					breakpoint: 1024,
					settings: {
						arrows: false,
						centerMode: true,
						dots: true,
						slidesToShow: 2,
					},
				},
				{
					breakpoint: 640,
					settings: {
						arrows: false,
						centerMode: true,
						dots: true,
						slidesToShow: 1,
					},
				},
			],
		} );
	},
	initFeaturedProductCarousel() {
		const $feat_recipe = $( '.js-carousel-products' );

		if ( 0 === $feat_recipe.length ) {
			return;
		}

		MediaQuery._init();
		if ( MediaQuery.is( 'small only' ) ) {
			$feat_recipe.slick( {
				slidesToShow: 1,
				slidesToScroll: 1,
				centerMode: true,
				dots: true,
			} );
		}

		$( window ).on( 'changed.zf.mediaquery', function( event, newSize ) {
			if ( 'small' === newSize ) {
				$feat_recipe.slick( {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true,
					dots: true,
				} );
			}
			else {
				if ( $feat_recipe.hasClass( 'slick-slider' ) ) {
					$feat_recipe.slick( 'unslick' );
				}
			}
		} );

	},
	initFeaturedRecipeCarousel() {
		const $feat_recipe = $( '.js-carousel-feat-recipe' );

		if ( 0 === $feat_recipe.length ) {
			return;
		}

		MediaQuery._init();
		if ( MediaQuery.is( 'small only' ) ) {
			$feat_recipe.slick( {
				slidesToShow: 1,
				slidesToScroll: 1,
				centerMode: true,
				dots: true,
			} );
		}

		$( window ).on( 'changed.zf.mediaquery', function( event, newSize ) {
			if ( 'small' === newSize ) {
				$feat_recipe.slick( {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: true,
					dots: true,
				} );
			}
			else {
				if ( $feat_recipe.hasClass( 'slick-slider' ) ) {
					$feat_recipe.slick( 'unslick' );
				}
			}
		} );

	},
	initProductCarousels() {
		const $prod_full  = $( '.js-gallery-fullsize' );
		const $prod_thumb = $( '.js-gallery-thumbs' );

		// On first load we init the Carousels appropriately for the current
		// breakpoint
		MediaQuery._init();
		if ( MediaQuery.is( 'large' ) ) {
			$prod_thumb.slick( {
				slidesToShow: 5,
				slidesToScroll: 1,
				arrows: false,
				centerMode: true,
				asNavFor: '.js-gallery-fullsize',
				focusOnSelect: true,
				vertical: true,
				verticalSwiping: true,
			} );
			$prod_full.slick( {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				fade: true,
				asNavFor: '.js-gallery-thumbs',
			} );
		}
		else {
			$prod_full.slick( {
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: true,
				fade: true,
			} );
		}

		// On breakpoint events we re-init the carousels.
		$( window ).on( 'changed.zf.mediaquery', function( event, newSize ) {
			if ( 'small' === newSize || 'medium' === newSize ) {
				if ( $prod_thumb.hasClass( 'slick-slider' ) ) {
					$prod_thumb.slick( 'unslick' );
					$prod_full.slick( 'unslick' );

					$prod_full.slick( {
						slidesToShow: 1,
						slidesToScroll: 1,
						arrows: true,
						fade: true,
					} );
				}
			}
			else {
				$prod_thumb.slick( 'unslick' );
				$prod_full.slick( 'unslick' );

				$prod_thumb.slick( {
					slidesToShow: 5,
					slidesToScroll: 1,
					arrows: false,
					centerMode: true,
					asNavFor: '.js-gallery-fullsize',
					focusOnSelect: true,
					vertical: true,
					verticalSwiping: true,
				} );
				$prod_full.slick( {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: true,
					fade: true,
					asNavFor: '.js-gallery-thumbs',
				} );

			}
		} );
	},
};
