import 'jquery';
import Cookies from 'js-cookie';

const cookie = 'usstove-pencil-ad';

export default {
  init() {
    const pencil_ad = $('.js-pencil-ad');
    const uid = pencil_ad.data('uid');
    const dismissed = Cookies.get(cookie);

    if (dismissed !== uid) {
      pencil_ad.show();
    }

    $('.js-pencil-ad-close').click(function() {
      Cookies.set(cookie, uid);
      pencil_ad.hide();
    });
  },
};
