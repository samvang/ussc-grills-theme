<?php
/**
 * Filename carousel-products.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$products = get_field( 'featured_products' );
?>
<section class="c-feat-products">
	<style>
		.c-feat-products {
			background-image: url('<?php echo USSC\Theme\Assets\asset_path( 'images/feature-irondale-3-500px.png' ); ?>');
		}
		@media (min-width: 640px) {
			.c-feat-products {
				background-image: url('<?php echo USSC\Theme\Assets\asset_path( 'images/feature-irondale-3.png' ); ?>');
			}
		}
	</style>
	<?php if ( $products ) : ?>
		<ul class="c-feat-products__cards js-carousel-products">

			<?php foreach ( $products as $product ) : ?>
				<?php $product = wc_get_product( $product['product'] ); ?>
				<div class="cards__card-wrap">
					<a class="c-card--prod" href="<?php echo esc_attr( $product->get_permalink() ); ?>">
						<div class="c-card__thumb">
							<?php echo wp_kses_post( $product->get_image( 'woocommerce_thumbnail' ) ); ?>
							<div class="thumb__overlay">
								<button class="button hollow white">
									<?php esc_html_e( 'View Grill', 'ussc' ); ?>
								</button>
							</div>
						</div>
						<div class="c-card__content">
							<h2 class="content__title"><?php echo wp_kses_post( $product->get_title() ); ?></h2>
						</div>
					</a>
				</div>
			<?php endforeach; ?>

		</ul>
	<?php endif; ?>
</section>
