<?php
/**
 * Filename brands.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$our_brands = get_field( 'our_brands' );
?>
<section class="c-brands">
	<div class="row">
		<div class="c-brands__intro column small-12">
			<?php if ( ! empty( $our_brands['title'] ) ) : ?>
				<h2 class="intro__title"><?php echo esc_html( $our_brands['title'] ); ?></h2>
			<?php endif; ?>
		</div>
	</div>
	<?php if ( $our_brands['brands'] ) : ?>
		<div class="row column">
			<ul class="c-brands__list align-center">
				<?php foreach ( $our_brands['brands'] as $brand ) : ?>
					<li class="c-brands__item">
						<a class="" href="<?php echo esc_attr( $brand['link'] ); ?>">
							<?php echo wp_get_attachment_image( $brand['logo']['ID'], 'brand-logo' ); ?>
							<h3><?php echo esc_html( $brand['name'] ); ?></h3>
						</a>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>

</section>
