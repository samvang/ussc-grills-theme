<?php
/**
 * Filename carousel-home.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$carousel = get_field( 'carousel' );
?>
<section class="c-carousel--home">
	<div class="row--wide column">
		<ul class="c-carousel__container js-carousel-home">
			<?php foreach ( $carousel['slides'] as $key => $slide ) : ?>
				<li
						class="c-carousel__slide"
						style="background-image: url(<?php echo esc_url_raw( wp_get_attachment_image_url( $slide['background_image']['ID'], 'full' ) ); ?>)"
				>
					<div class="u-valign-wrap">
						<div class="u-valign-content">
							<div class="row">
								<div class="column small-12 medium-10 medium-offset-1">
									<?php if ( ! empty( $slide['small_text'] ) ) : ?>
										<p class="small"><?php echo wp_kses_post( $slide['small_text'] ); ?></p>
									<?php endif; ?>
									<?php if ( ! empty( $slide['title'] ) ) : ?>
										<h1 class="title"><?php echo wp_kses_post( $slide['title'] ); ?></h1>
									<?php endif; ?>
									<?php if ( ! empty( $slide['button_link'] ) ) : ?>
										<a href="<?php echo esc_attr( $slide['button_link'] ); ?>" class="hollow button">
											<?php echo esc_html( $slide['button_label'] ); ?>
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</section>
