<?php
/**
 * Filename carousel-categories.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$category_terms = get_field( 'featured_product_categories' );
?>
<section class="c-carousel--cats">
	<div class="row collapse">
		<ul class="c-carousel__container js-carousel-cats column">
			<?php foreach ( $category_terms as $key => $value ) : ?>
				<li class="c-carousel__slide">
					<a href="<?php echo esc_attr( get_term_link( $value['category'], 'product_cat' ) ); ?>">
						<div class="c-card--cat">
							<div class="c-card__thumb">
								<?php woocommerce_subcategory_thumbnail( $value['category'] ); ?>
								<div class="thumb__overlay">
									<button class="button hollow white">
										<?php esc_html_e( 'View Products', 'ussc' ); ?>
									</button>
								</div>
							</div>
							<div class="c-card__content">
								<h2 class="content__title"><?php echo wp_kses_post( $value['category']->name ); ?></h2>
							</div>
						</div>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</section>
