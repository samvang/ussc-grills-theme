<?php
/**
 * Filename page-header.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>

<?php use USSC\Theme\Titles; ?>

<h1 class="page-header"><?php echo Titles\title(); // phpcs:ignore ?></h1>
