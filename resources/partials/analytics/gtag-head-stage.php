<?php
/**
 * Filename gtag-head-staging.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<!-- STAGE: Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=j4wyaK-6uuIC00C-H-85Dw&gtm_preview=env-5&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TB3F6ZS');</script>
<!-- End Google Tag Manager -->
