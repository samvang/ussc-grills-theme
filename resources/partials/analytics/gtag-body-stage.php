<?php
/**
 * Filename gtag-body-staging.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<!-- STAGE: Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TB3F6ZS&gtm_auth=j4wyaK-6uuIC00C-H-85Dw&gtm_preview=env-5&gtm_cookies_win=x"
				  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
