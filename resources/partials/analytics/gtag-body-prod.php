<?php
/**
 * Filename gtag-live-body.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<!-- LIVE: Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TB3F6ZS"
				  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
