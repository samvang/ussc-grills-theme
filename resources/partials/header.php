<?php
/**
 * Filename header.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use USSC\Theme;

$logo           = get_field( 'header_logo', 'option' );
$logo_image_url = wp_get_attachment_image_url( $logo['ID'], 'full' );
?>
<header class="l-header show-for-large">
	<nav class="c-nav-top">
		<div class="row--wide column align-justify">
			<ul id="menu-top-navigation-left" class="c-nav-top__l">
			</ul>
			<ul id="menu-top-navigation-right" class="c-nav-top__r">
				<li class="menu-item menu-my-account">
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>"><?php esc_html_e( 'My Account', 'usstove' ); ?></a>
				</li>
				<li>
					<a class="c-cart-menu" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'usstove' ); ?>">
						<?php
						$cart_count = absint( WC()->cart->get_cart_contents_count() );
						if ( 0 === $cart_count ) {
							esc_html_e( 'Shopping Cart', 'usstove' );
						} else {
							echo sprintf(
								_n(
									'Shopping Cart - %d item',
									'Shopping Cart - %d items',
									absint( WC()->cart->get_cart_contents_count() )
								),
								absint( WC()->cart->get_cart_contents_count() )
							);
						}
						?>
					</a>
				</li>
				<?php if ( false ) : ?>
					<li class="menu-item menu-search-usstove-com">
						<a href="#" data-open="search">
							<?php esc_html_e( 'Search USSCGrills.com', 'usstove' ); ?>
						</a>
					</li>
				<?php endif; ?>
			</ul>
		</div>
	</nav>
	<nav class="c-nav-main">
		<div class="row--wide column">
			<div class="c-nav-main__brand">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<span class="brand__link" style="background-image: url(<?php echo esc_url( $logo_image_url ); ?>)">
						<?php bloginfo( 'name' ); ?>
					</span>
				</a>
			</div>
			<?php if ( has_nav_menu( 'main_nav_l' ) ) : ?>
				<?php
				wp_nav_menu( [
					'theme_location' => 'main_nav_l',
					'menu_class'     => 'c-nav-main__l js-mega-menu',
					'walker'         => new Theme\Mega_Nav_Walker(),
				] );
				?>
			<?php endif; ?>
			<?php if ( has_nav_menu( 'main_nav_r' ) ) : ?>
				<?php
				wp_nav_menu( [
					'theme_location' => 'main_nav_r',
					'menu_class'     => 'c-nav-main__r',
				] );
				?>
			<?php endif; ?>
			<?php get_template_part( 'partials/mega-menu-parts' ); ?>
			<?php get_template_part( 'partials/mega-menu-products' ); ?>
		</div>
	</nav>
</header>
