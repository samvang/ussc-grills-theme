<?php
/**
 * Filename content-single.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
	<section class="c-carousel--recipes">
		<div class="row--wide column">
			<ul class="c-carousel__container js-carousel-recipes">

				<li
					class="c-carousel__slide"
					style="background-image: url(<?php echo esc_url_raw( wp_get_attachment_image_url( get_post_thumbnail_id(), 'full' ) ); ?>)"
				>
					<div class="row">
						<div class="column small-12 medium-5">
							<h2 class="title"><?php the_title(); ?></h2>
						</div>
					</div>

				</li>
			</ul>
		</div>
	</section>

	<div class="row">
		<div class="column small-12">
			<article <?php post_class(); ?>>
				<header>
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
				<div class="entry-content">
					<div class="row">
						<div class="column small-12">
							<?php the_content(); ?>
						</div>
					</div>

				</div>
				<footer>
					<?php
					wp_link_pages(
						[
							'before' => '<nav class="page-nav"><p>' . __( 'Pages:', 'usstove' ),
							'after'  => '</p></nav>',
						]
					);
					?>
				</footer>
			</article>
		</div>
		<aside class="column small-12 medium-4">
			<?php
			if ( function_exists( 'sharing_display' ) ) {
				sharing_display( '', true );
			}

			if ( class_exists( 'Jetpack_Likes' ) ) {
				$custom_likes = new Jetpack_Likes;
				echo $custom_likes->post_likes( '' );
			}
			?>
		</aside>
	</div>
<?php endwhile;

