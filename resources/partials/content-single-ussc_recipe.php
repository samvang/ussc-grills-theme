<?php
/**
 * Filename content-single.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
	<section class="c-carousel--recipes">
		<div class="row--wide column">
			<ul class="c-carousel__container js-carousel-recipes">

				<li
					class="c-carousel__slide"
					style="background-image: url(<?php echo esc_url_raw( wp_get_attachment_image_url( get_post_thumbnail_id(), 'full' ) ); ?>)"
				>
					<div class="row">
						<div class="column small-12 medium-5">
							<h2 class="title"><?php the_title(); ?></h2>
						</div>
					</div>

				</li>
			</ul>
		</div>
	</section>

	<?php
	$categories = get_terms( [
		'taxonomy' => 'ussc_recipe_cat',
	] );
	?>
	<section class="c-category-nav">
		<div class="row small-up-1 medium-up-3">
			<?php foreach ( $categories as $category ) : ?>
				<div class="column">
					<div class="nav-item align-middle align-center">
						<?php printf( '<a class="nav-link" href="%s">%s</a>', esc_attr( get_category_link( $category ) ), esc_html( $category->name ) ); ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</section>


	<div class="row">
		<div class="column small-12">
			<article <?php post_class(); ?>>
				<header>
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
				<div class="entry-content">
					<div class="row">
						<?php $gallery = get_field( 'gallery' ); ?>
						<?php if ( empty( $gallery ) ) : ?>
							<div class="column small-12 medium-8">
								<?php the_content(); ?>
							</div>
						<?php else : ?>
							<div class="column small-12 medium-6">
								<?php the_content(); ?>
							</div>
							<div class="column small-12 medium-6">
								<div class="recipe-gallery">
									<?php
									$gallery = get_field( 'gallery' );
									if ( is_array( $gallery ) ) {
										foreach ( $gallery as $image ) {
											echo wp_get_attachment_image( $image['ID'], 'large', false, [ 'class' => 'recipe-gallery-image' ] );
										}
									}
									?>
								</div>
							</div>
						<?php endif; ?>
					</div>

				</div>
				<footer>
					<?php
					wp_link_pages(
						[
							'before' => '<nav class="page-nav"><p>' . __( 'Pages:', 'usstove' ),
							'after'  => '</p></nav>',
						]
					);
					?>
				</footer>
			</article>
		</div>
		<aside class="column small-12 medium-4">
			<?php
			if ( function_exists( 'sharing_display' ) ) {
				sharing_display( '', true );
			}

			if ( class_exists( 'Jetpack_Likes' ) ) {
				$custom_likes = new Jetpack_Likes;
				echo $custom_likes->post_likes( '' );
			}
			?>
		</aside>
	</div>
<?php endwhile;

