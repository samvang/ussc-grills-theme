<?php
/**
 * Filename browse-happy.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<!--[if IE]>
<div class="alert alert-warning">
	<?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
	browser</a> to improve your experience.', 'usstove'); ?>
</div>
<![endif]-->

