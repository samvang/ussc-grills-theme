<?php
/**
 * Filename mega-menu-parts.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$menu = get_field( 'mega_menu_parts', 'option' );

?>
<div id="mega-menu-parts" class="c-mega-menu__panel--parts js-mega-menu-panel">
	<div class="row--wide">


		<?php if ( ! empty( $menu['fields_column_1'] ) ) : ?>
			<?php $col1 = $menu['fields_column_1']; ?>
			<div class="large-4 column panel__parts">
				<div class="row--4 collapse">
					<?php $title_term = get_term( $col1['title_product_cat'], 'product_cat' ); ?>
					<?php if ( $title_term instanceof WP_Term ) : ?>
						<div class="col-1">
							<?php
							printf( '<a href="%s">%s</a>',
								esc_attr( get_term_link( $title_term->term_id ) ),
								wp_get_attachment_image( $col1['image'], 'mega-menu-thumb' )
							);
							?>
						</div>
						<div class="col-3">
							<h3 class="panel__title">
								<?php
								printf( '<a href="%s">%s</a>',
									esc_attr( get_term_link( $title_term->term_id ) ),
									esc_html( $col1['title'] )
								);
								?>
							</h3>
							<p class="panel__description">
								<?php echo esc_html( $col1['description'] ); ?>
							</p>
							<?php if ( $col1['product_cats'] ) : ?>
								<ul class="panel__list">
									<?php foreach ( $col1['product_cats'] as $cat_group ) : ?>
										<li class="panel__list-item">
											<?php
											$term = get_term( $cat_group['product_cat'], 'product_cat' );
											printf( '<a href="%s">%s</a>',
												esc_attr( get_term_link( $term->term_id ) ),
												esc_html( $term->name )
											);
											?>
										</li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>


		<?php if ( ! empty( $menu['fields_column_2'] ) ) : ?>
			<?php $col2 = $menu['fields_column_2']; ?>
			<div class="large-4 column panel__accessories">
				<div class="row--4 collapse">
					<?php $title_term = get_term( $col2['title_product_cat'], 'product_cat' ); ?>
					<?php if ( $title_term instanceof WP_Term ) : ?>
						<div class="col-1">
							<?php
							printf( '<a href="%s">%s</a>',
								esc_attr( get_term_link( $title_term->term_id ) ),
								wp_get_attachment_image( $col2['image'], 'mega-menu-thumb' )
							);
							?>
						</div>
						<div class="col-3">
							<h3 class="panel__title">
								<?php
								printf( '<a href="%s">%s</a>',
									esc_attr( get_term_link( $title_term->term_id ) ),
									esc_html( $col2['title'] )
								);
								?>
							</h3>
							<p class="panel__description">
								<?php echo esc_html( $col2['description'] ); ?>
							</p>
							<?php if ( $col2['product_cats'] ) : ?>
								<ul class="panel__list">
									<?php foreach ( $col2['product_cats'] as $cat_group ) : ?>
										<li class="panel__list-item">
											<?php
											$term = get_term( $cat_group['product_cat'], 'product_cat' );
											printf( '<a href="%s">%s</a>',
												esc_attr( get_term_link( $term->term_id ) ),
												esc_html( $term->name )
											);
											?>
										</li>
									<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>


		<?php if ( ! empty( $menu['fields_column_3'] ) ) : ?>
			<?php $col3 = $menu['fields_column_3']; ?>
			<div class="large-4 column panel__actions">
				<h3 class="panel__title">
					<?php echo esc_html( $col3['support_title'] ); ?>
				</h3>
				<div class="row collapse">
					<div class="medium-4 column">
						<p><strong>Call us</strong></p>
						<p>1-800-750-2723</p>
					</div>
					<div class="medium-4 column">
						<p><strong>Text us</strong></p>
						<p>1-423-436-7054</p>
					</div>
					<div class="medium-4 column">
						<p><strong>Locate a dealer</strong></p>
						<p><a href="#">Store Locator</a></p>
					</div>
				</div>
			</div>
		<?php endif; ?>


	</div>
</div>
