<?php
/**
 * Filename product_cat-meta-display-msrp-on-price.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

?>
<div class="form-field term-display-msrp-on-price-wrap">
	<label for="tag-display-msrp-on-price">
		<input name="ussc_display_msrp_on_price" id="tag-display-msrp-on-price" type="checkbox" value="on">
		<?php esc_html_e( 'Display MSRP on Price', 'usstove' ); ?>
	</label>
	<p class="description">
		<?php esc_html_e( 'Products in this category will have a small label shown under the price saying "Suggested MSRP". Note: this applies only to the selected category, you must repeat this action for any child categories.', 'usstove' ); ?>
	</p>
</div>
