<?php
/**
 * Filename nav-mobile-top.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */
?>
<li class="menu-item menu-item--top" role="treeitem">
	<span class="menu-item__link">
		<?php esc_html_e( 'Welcome to USSCGrills.com', 'ussc' ); ?>
		<button class="close-button" aria-label="Close menu" type="button" data-close>
			<span aria-hidden="true">&times;</span>
		</button>
	</span>
</li>
