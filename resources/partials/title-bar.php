<?php
/**
 * Filename title-bar.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$logo           = get_field( 'header_logo_mobile', 'option' );
$logo_image_url = wp_get_attachment_image_url( $logo['ID'], 'full' );
?>
<div class="c-title-bar title-bar hide-for-large">
	<div class="title-bar-left c-title-bar__brand">
		<a
				class="brand__link"
				href="<?php echo esc_url( home_url( '/' ) ); ?>"
				style="background-image: url(<?php echo esc_url( $logo_image_url ); ?>)"
		>
			<?php bloginfo( 'name' ); ?>
		</a>
	</div>
	<div class="title-bar-right">
		<a class="c-title-bar__search" type="button" data-open="search"></a>
		<button class="c-title-bar__nav" type="button" data-open="OffCanvasNav"></button>
	</div>
</div>
