<?php
/**
 * Filename modal-where-to-buy.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$retailers = get_field( 'retailers', 'option' );
?>

<?php if ( is_array( $retailers ) && count( $retailers ) ) : ?>
	<div class="c-reveal reveal large" id="where-to-buy" data-reveal>
		<button class="close-button" data-close aria-label="Close modal" type="button">
			<span aria-hidden="true">&times;</span>
		</button>
		<div class="row">
			<h2 class="reveal__title"><?php esc_html_e( 'Our Valued Retailers', 'usstove' ); ?></h2>
		</div>
		<div class="c-where-to-buy">
			<?php foreach ( $retailers as $retailer ) : ?>
				<div class="where-to-buy__retailer">
					<a target="_blank" href="<?php echo ( $retailer['url'] ) ? esc_url( $retailer['url'] ) : '#'; ?>" data-retailer-name="<?php echo esc_attr( $retailer['name'] ); ?>">
						<?php echo wp_get_attachment_image( $retailer['logo']['ID'], 'small' ); ?>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php endif;
