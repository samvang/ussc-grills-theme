<?php
/**
 * Filename footer.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

$social_links = [];

$social_links['facebook']  = get_field( 'facebook_url', 'option' );
$social_links['twitter']   = get_field( 'twitter_url', 'option' );
$social_links['instagram'] = get_field( 'instagram_url', 'option' );
$social_links['youtube']   = get_field( 'youtube_url', 'option' );

$social_links = array_filter( $social_links );

?>
<footer>
		<section class="c-footer__support">
			<div class="row">
				<div class="support__col"><h2 class="support__title"><?php the_field( 'footer_support_heading', 'option' ); ?></h2></div>
				<div class="support__col"><?php the_field( 'footer_support_col_1', 'option' ); ?></div>
				<div class="support__col"><?php the_field( 'footer_support_col_2', 'option' ); ?></div>
				<!--
				<div class="support__col">
					<?php the_field( 'footer_support_col_3', 'option' ); ?>
				</div>
				-->
			</div>
		</section>
		<section class="c-footer__subscribe">
			<div class="row">
				<div class="subscribe__copy column small-12 medium-6 large-4 large-offset-2">
					<h2 class="subscribe__title"><?php the_field( 'footer_subscribe_heading', 'option' ); ?></h2>
					<?php the_field( 'footer_subscribe_content', 'option' ); ?>
				</div>
				<div class="subscribe__form column small-12 medium-6 large-4 large-offset-2">
					<?php echo do_shortcode( '[contact-form-7 id="447" title="Mailing List Subscription Form"]' ); ?>
				</div>
			</div>
		</section>
		<section class="c-footer__about">
			<div class="row">
				<div class="column small-10 small-offset-1 medium-offset-0 medium-8">
					<?php
					$footer_logo = get_field( 'footer_about_logo', 'option' );
					?>
					<h2
							class="about__brand"
							style="background-image: url(<?php echo wp_get_attachment_image_url( $footer_logo['ID'], 'full' ); ?>)"
					>
						<?php bloginfo( 'name' ); ?>
					</h2>
					<div class="about__content">
						<?php the_field( 'footer_about_content', 'option' ); ?>
					</div>
				</div>
				<div class="column small-12 medium-3 medium-offset-1">
					<div class="about__social">
						<div class="social__title">
							<span><?php esc_html_e( 'Follow Us', 'ussc' ); ?></span>
						</div>
						<?php if ( ! empty( $social_links ) ) : ?>
							<div class="social__links">
								<ul>
									<?php if ( isset( $social_links['facebook'] ) ) : ?>
										<li>
											<a href="<?php echo esc_attr( $social_links['facebook'] ); ?>">
												<i class="usstove-icon_social_Facebook"></i>
											</a>
										</li>
									<?php endif; ?>
									<?php if ( isset( $social_links['twitter'] ) ) : ?>
										<li>
											<a href="<?php echo esc_attr( $social_links['twitter'] ); ?>">
												<i class="usstove-icon_social_Twitter"></i>
											</a>
										</li>
									<?php endif; ?>
									<?php if ( isset( $social_links['instagram'] ) ) : ?>
										<li>
											<a href="<?php echo esc_attr( $social_links['instagram'] ); ?>">
												<i class="usstove-icon_social_Instagram"></i>
											</a>
										</li>
									<?php endif; ?>
									<?php if ( isset( $social_links['youtube'] ) ) : ?>
										<li>
											<a href="<?php echo esc_attr( $social_links['youtube'] ); ?>">
												<i class="usstove-icon_social_Youtube"></i>
											</a>
										</li>
									<?php endif; ?>
								</ul>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php if ( false ) : ?>
		<section class="c-footer__sitemap">
			<div class="row">
				<div class="column small-12 medium-3">
					<h3 class="sitemap__title"><?php the_field( 'footer_sitemap_col_1', 'option' ); ?></h3>
					<?php if ( has_nav_menu( 'footer_nav_1' ) ) : ?>
						<?php
						wp_nav_menu( [
							'theme_location' => 'footer_nav_1',
							'menu_class'     => 'c-nav-footer',
						] );
						?>
					<?php endif; ?>
				</div>
				<div class="column small-12 medium-3">
					<h3 class="sitemap__title"><?php the_field( 'footer_sitemap_col_2', 'option' ); ?></h3>
					<?php if ( has_nav_menu( 'footer_nav_2' ) ) : ?>
						<?php
						wp_nav_menu( [
							'theme_location' => 'footer_nav_2',
							'menu_class'     => 'c-nav-footer',
						] );
						?>
					<?php endif; ?>
				</div>
				<div class="column small-12 medium-3">
					<h3 class="sitemap__title"><?php the_field( 'footer_sitemap_col_3', 'option' ); ?></h3>
					<?php if ( has_nav_menu( 'footer_nav_3' ) ) : ?>
						<?php
						wp_nav_menu( [
							'theme_location' => 'footer_nav_3',
							'menu_class'     => 'c-nav-footer',
						] );
						?>
					<?php endif; ?>

				</div>
				<div class="column small-12 medium-3">
					<h3 class="sitemap__title"><?php the_field( 'footer_sitemap_col_4', 'option' ); ?></h3>
					<?php if ( has_nav_menu( 'footer_nav_4' ) ) : ?>
						<?php
						wp_nav_menu( [
							'theme_location' => 'footer_nav_4',
							'menu_class'     => 'c-nav-footer',
						] );
						?>
					<?php endif; ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<section class="c-footer__copyright">
		<p><?php the_field( 'footer_copyright', 'option' ); ?></p>
	</section>
</footer>
