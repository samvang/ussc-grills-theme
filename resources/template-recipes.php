<?php
/**
 * Filename template-recipes.php
 *
 * Template Name: Recipes Template
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */

use USSC\Theme\Recipes;

$carousel_posts = Recipes\get_carousel_posts();

?>
<section class="c-carousel--recipes">
	<div class="row--wide column">
		<ul class="c-carousel__container js-carousel-recipes">
			<?php foreach ( $carousel_posts as $carousel_post ) : ?>
				<li
					class="c-carousel__slide"
					style="background-image: url(<?php echo esc_attr( get_the_post_thumbnail_url( $carousel_post, 'full' ) ); ?>)"
				>
					<div class="row">
						<div class="column small-12 medium-5">
							<h2 class="title"><?php echo esc_html( $carousel_post->post_title ); ?></h2>
							<a href="<?php echo esc_attr( get_the_permalink( $carousel_post->ID ) ); ?>" class="button">
								<?php esc_html_e( 'View Recipe', 'usstove' ); ?>
							</a>
						</div>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</section>

<?php
$categories = get_terms( [
	'taxonomy' => 'ussc_recipe_cat',
] );
?>
<section class="c-category-nav">
	<div class="row small-up-1 medium-up-3">
		<?php foreach ( $categories as $category ) : ?>
			<div class="column">
				<div class="nav-item align-middle align-center">
					<?php printf( '<a class="nav-link" href="%s">%s</a>', esc_attr( get_category_link( $category ) ), esc_html( $category->name ) ); ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>

<header>
	<div class="row column">
		<?php the_title( '<h1 class="text-center">', '</h1>' ); ?>
	</div>
</header>

<?php
$featured_posts = Recipes\get_featured_posts();
?>
<div class="l-card-grid">
	<?php foreach ( $featured_posts as $post ) : ?>
		<?php setup_postdata( $post ); ?>
		<div class="column">
			<?php get_template_part( 'partials/content', 'post' ); ?>
		</div>
	<?php endforeach; ?>
</div>
