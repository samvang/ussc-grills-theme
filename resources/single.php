<?php
/**
 * Filename single.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */
?>

<?php get_template_part('partials/content-single', get_post_type()); ?>
