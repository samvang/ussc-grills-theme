<?php
/**
 * Filename search.php
 *
 * @package ussc
 * @author  Peter Toi <peter@petertoi.com>
 */
?>

<?php get_template_part( 'partials/page', 'header' ); ?>

<div class="row">
	<div class="column small-12">
		<?php if ( ! have_posts() ) : ?>
			<div class="alert alert-warning">
				<?php esc_html_e( 'Sorry, no results were found.', 'usstove' ); ?>
			</div>
			<?php get_product_search_form(); ?>
		<?php endif; ?>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<?php get_template_part( 'partials/content', 'search' ); ?>
		<?php endwhile; ?>
	</div>
</div>

<?php the_posts_navigation(); ?>
